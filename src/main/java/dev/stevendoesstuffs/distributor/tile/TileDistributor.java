package dev.stevendoesstuffs.distributor.tile;

import dev.stevendoesstuffs.distributor.DistributorConfig;
import dev.stevendoesstuffs.distributor.Util;
import dev.stevendoesstuffs.distributor.container.FluidQueue;
import dev.stevendoesstuffs.distributor.container.ItemQueue;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

import javax.annotation.Nullable;
import java.util.*;

public class TileDistributor extends TileEntity implements ITickable {

    private final EnumMap<EnumFacing, Boolean> outputs;
    private ItemQueue items;
    private FluidQueue fluids;

    public TileDistributor() {
        outputs = new EnumMap<>(EnumFacing.class);
        outputs.put(EnumFacing.UP, false);
        outputs.put(EnumFacing.DOWN, false);
        outputs.put(EnumFacing.NORTH, true);
        outputs.put(EnumFacing.SOUTH, true);
        outputs.put(EnumFacing.EAST, true);
        outputs.put(EnumFacing.WEST, true);

        items = new ItemQueue(DistributorConfig.itemQueueTypes, DistributorConfig.itemQueueCapacity) {
            @Override
            protected void updateParent() {
                markDirty();
            }
        };
        fluids = new FluidQueue(DistributorConfig.fluidQueueTypes, DistributorConfig.fluidQueueCapacity) {
            @Override
            protected void updateParent() {
                markDirty();
            }
        };
    }

    public EnumMap<EnumFacing, Boolean> getOutputs() {
        return outputs;
    }

    public void dropItems(World world, BlockPos pos) {
        items.dropItems(world, pos);
    }

    public NBTTagCompound writeSidesToNBT(NBTTagCompound compound) {
        NBTTagCompound outputsNBT = new NBTTagCompound();
        for (Map.Entry<EnumFacing, Boolean> side : outputs.entrySet()) {
            outputsNBT.setBoolean(side.getKey().toString().toLowerCase(), side.getValue());
        }
        compound.setTag("outputs", outputsNBT);
        return compound;
    }

    public void readSidesFromNBT(NBTTagCompound compound) {
        NBTTagCompound outputsNBT = compound.getCompoundTag("outputs");
        for (EnumFacing side : EnumFacing.values()) {
            outputs.put(side, outputsNBT.getBoolean(side.toString().toLowerCase()));
        }
    }

    public void flipSide(EnumFacing face) {
        outputs.put(face, !outputs.get(face));
        markDirty();
        notifyBlockUpdate();
    }

    private void notifyBlockUpdate() {
        IBlockState state = getWorld().getBlockState(getPos());
        getWorld().notifyBlockUpdate(getPos(), state, state, 3);
    }



    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        writeSidesToNBT(compound);
        compound.setTag("items", items.serializeNBT());
        compound.setTag("fluids", fluids.serializeNBT());
        return compound;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        readSidesFromNBT(compound);
        items.deserializeNBT(compound.getCompoundTag("items"));
        fluids.deserializeNBT(compound.getCompoundTag("fluids"));
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        return writeSidesToNBT(super.getUpdateTag());
    }

    @Override
    public void handleUpdateTag(NBTTagCompound nbt) {
        super.handleUpdateTag(nbt);
        readSidesFromNBT(nbt);
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(getPos(), 0, writeSidesToNBT(new NBTTagCompound()));
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        readSidesFromNBT(pkt.getNbtCompound());
        notifyBlockUpdate();
    }

    @Override
    public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState) {
        return oldState.getBlock() != newState.getBlock();
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ||
            capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY ||
            super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) return (T) items;
        else if (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY) return (T) fluids;
        else return super.getCapability(capability, facing);
    }

    private <T> List<T> getNeighbors(Capability<T> capability) {
        List<T> neighbors = new ArrayList<>();
        for (EnumFacing face : EnumFacing.values()) {
            if (!outputs.get(face)) continue;
            TileEntity tile = getWorld().getTileEntity(getPos().offset(face));
            T handler;
            if (tile == null || !tile.hasCapability(capability, face.getOpposite()) ||
                (handler = tile.getCapability(capability, face.getOpposite()))
                    == this.getCapability(capability, face.getOpposite())) return Collections.emptyList();
            neighbors.add(handler);
        }
        return neighbors;
    }

    private void pushItems() {
        if (items.getCurrentCount() == 0) return;
        List<IItemHandler> inventories = getNeighbors(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY);
        if (inventories.isEmpty()) return;

        while (items.getCurrentCount() >= inventories.size()) {
            int max = Math.min(items.getCurrentCount() / inventories.size(),
                items.getStackInSlot(0).getMaxStackSize());
            for (IItemHandler inv : inventories) {
                max = Math.min(max, max - ItemHandlerHelper.insertItemStacked(inv,
                    ItemHandlerHelper.copyStackWithSize(items.getStackInSlot(0), max), true).getCount());
            }

            if (max == 0) break;
            for (IItemHandler inv : inventories) {
                ItemHandlerHelper.insertItemStacked(inv, ItemHandlerHelper
                    .copyStackWithSize(items.getStackInSlot(0), max), false);
                items.extractItem(0, max, false);
            }
        }
    }

    private void pushFluids() {
        if (fluids.getCurrentAmount() == 0) return;
        List<IFluidHandler> tanks = getNeighbors(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY);
        if (tanks.isEmpty()) return;

        while (fluids.getCurrentAmount() >= tanks.size()) {
            int max = fluids.getCurrentAmount() / tanks.size();
            for (IFluidHandler tank : tanks) {
                max = Math.min(max, tank.fill(fluids.copyCurrentFluid(max), false));
            }

            if (max == 0) break;
            FluidStack fluid = fluids.copyCurrentFluid(max);
            for (IFluidHandler tank : tanks) {
                tank.fill(fluid, true);
                fluids.drain(max, true);
            }
        }
    }

    @Override
    public void update() {
        pushItems();
        pushFluids();
    }
}
