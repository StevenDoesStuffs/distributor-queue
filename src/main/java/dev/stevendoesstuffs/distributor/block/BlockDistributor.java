package dev.stevendoesstuffs.distributor.block;

import dev.stevendoesstuffs.distributor.Distributor;
import dev.stevendoesstuffs.distributor.Util;
import dev.stevendoesstuffs.distributor.tile.TileDistributor;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockDistributor extends Block {

    public static final PropertyBool OUTPUT_NORTH = PropertyBool.create("output_north");
    public static final PropertyBool OUTPUT_SOUTH = PropertyBool.create("output_south");
    public static final PropertyBool OUTPUT_EAST = PropertyBool.create("output_east");
    public static final PropertyBool OUTPUT_WEST = PropertyBool.create("output_west");
    public static final PropertyBool OUTPUT_UP = PropertyBool.create("output_up");
    public static final PropertyBool OUTPUT_DOWN = PropertyBool.create("output_down");

    public BlockDistributor() {
        super(Material.ROCK);
        this.setSoundType(SoundType.STONE)
            .setUnlocalizedName(Distributor.MODID + ".distributor")
            .setRegistryName("distributor")
            .setHardness(2.5F)
            .setHarvestLevel("pickaxe", 0);
        setDefaultState(this.blockState.getBaseState()
            .withProperty(OUTPUT_NORTH, false)
            .withProperty(OUTPUT_SOUTH, false)
            .withProperty(OUTPUT_EAST, false)
            .withProperty(OUTPUT_WEST, false)
            .withProperty(OUTPUT_UP, false)
            .withProperty(OUTPUT_DOWN, false));
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer.Builder(this)
            .add(OUTPUT_NORTH)
            .add(OUTPUT_SOUTH)
            .add(OUTPUT_EAST)
            .add(OUTPUT_WEST)
            .add(OUTPUT_UP)
            .add(OUTPUT_DOWN)
            .build();
    }

    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        TileDistributor tile = (TileDistributor) worldIn.getTileEntity(pos);
        return state
            .withProperty(OUTPUT_NORTH, tile.getOutputs().get(EnumFacing.NORTH))
            .withProperty(OUTPUT_SOUTH, tile.getOutputs().get(EnumFacing.SOUTH))
            .withProperty(OUTPUT_EAST, tile.getOutputs().get(EnumFacing.EAST))
            .withProperty(OUTPUT_WEST, tile.getOutputs().get(EnumFacing.WEST))
            .withProperty(OUTPUT_UP, tile.getOutputs().get(EnumFacing.UP))
            .withProperty(OUTPUT_DOWN, tile.getOutputs().get(EnumFacing.DOWN));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return 0;
    }

    public boolean hasTileEntity(IBlockState state) {
        return true;
    }

    public TileEntity createTileEntity(World world, IBlockState state) {
        return new TileDistributor();
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player,
        EnumHand hand, EnumFacing hitFace, float hitX, float hitY, float hitZ) {

        TileEntity tile = world.getTileEntity(pos);

        if (world.isRemote) return true;
        else if (
            !(tile instanceof TileDistributor) ||
            !player.isSneaking() ||
            !player.getHeldItemMainhand().isEmpty() ||
            !player.getHeldItemOffhand().isEmpty() ||
            hand != EnumHand.MAIN_HAND) return false;

        float x = 0;
        float y = 0;

        if (hitX == 0.0) {
            x = hitZ;
            y = hitY;
        } else if (hitX == 1.0) {
            x = 1 - hitZ;
            y = hitY;
        } else if (hitY == 0.0) {
            x = hitX;
            y = hitZ;
        } else if (hitY == 1.0) {
            x = hitX;
            y = 1 - hitZ;
        } else if (hitZ == 0.0) {
            x = 1 - hitX;
            y = hitY;
        } else if (hitZ == 1.0) {
            x = hitX;
            y = hitY;
        }

        x -= 0.5;
        y -= 0.5;

        int index;

        if (Math.max(Math.abs(x), Math.abs(y)) <= 3.0 / 16.0) index = 4;
        else if (Math.min(Math.abs(x), Math.abs(y)) >= 4.0 / 16.0) index = 5;
        else if (Math.abs(x) <= Math.abs(y)) index = y >= 0 ? 0 : 2;
        else index = x >= 0 ? 1 : 3;

        ((TileDistributor) tile).flipSide(Util.relative(hitFace)[index]);

        return true;
    }

    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state) {
        TileEntity tileRaw = world.getTileEntity(pos);
        if (tileRaw instanceof TileDistributor)
            ((TileDistributor) tileRaw).dropItems(world, pos);
        super.breakBlock(world, pos, state);
    }
}
