package dev.stevendoesstuffs.distributor;

import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = Distributor.MODID, useMetadata = true)

public class Distributor {
    public static final String MODID = "distributorqueue";
    public static final Logger LOGGER = LogManager.getLogger(Distributor.MODID);
}
