package dev.stevendoesstuffs.distributor;

import net.minecraft.util.EnumFacing;
import net.minecraftforge.fluids.FluidStack;

public class Util {
    private static EnumFacing faceUp(EnumFacing face) {
        switch (face) {
            case UP: return EnumFacing.NORTH;
            case DOWN: return EnumFacing.SOUTH;
            default: return EnumFacing.UP;
        }
    }

    private static EnumFacing faceRight(EnumFacing face) {
        switch (face) {
            case NORTH: return EnumFacing.WEST;
            case WEST: return EnumFacing.SOUTH;
            case DOWN:
            case UP:
            case SOUTH: return EnumFacing.EAST;
            case EAST:
            default: return EnumFacing.NORTH;
        }
    }

    public static EnumFacing[] relative(EnumFacing face) {
        return new EnumFacing[]{
            faceUp(face), faceRight(face),
            faceUp(face).getOpposite(), faceRight(face).getOpposite(),
            face, face.getOpposite()
        };
    }

    public static FluidStack copyFluidStackWithSize(FluidStack fluid, int amount) {
        FluidStack result = fluid.copy();
        result.amount = amount;
        return result;
    }
}

