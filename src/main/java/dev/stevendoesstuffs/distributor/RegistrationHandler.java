package dev.stevendoesstuffs.distributor;

import dev.stevendoesstuffs.distributor.block.BlockDistributor;
import dev.stevendoesstuffs.distributor.tile.TileDistributor;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod.EventBusSubscriber(modid = Distributor.MODID)
@GameRegistry.ObjectHolder(Distributor.MODID)
public class RegistrationHandler {
    public static final Block distributor = null;

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        event.getRegistry().register(new ItemBlock(distributor)
            .setRegistryName(distributor.getRegistryName()));
    }

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {
        GameRegistry.registerTileEntity(TileDistributor.class,
            new ResourceLocation(Distributor.MODID, "tileEntityDistributor"));
        event.getRegistry().register(new BlockDistributor());
    }
}
