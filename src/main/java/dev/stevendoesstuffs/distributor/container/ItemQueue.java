package dev.stevendoesstuffs.distributor.container;

import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

import javax.annotation.Nonnull;

public class ItemQueue implements IItemHandler, INBTSerializable<NBTTagCompound> {
    private RingBuffer<ItemStack> items;
    private int capacity, count;

    public ItemQueue(int length, int capacity) {
        this.items = new RingBuffer<>(length, ItemStack.EMPTY);
        this.capacity = capacity;
        this.count = 0;
    }

    public int getCurrentCount() {
        return items.isEmpty() ? 0 : items.get(0).getCount();
    }

    public void dropItems(World world, BlockPos pos) {
        for (int i = 0; i < items.size(); i++) {
            ItemStack itemstack = items.get(i);
            int stacks = itemstack.getCount() / itemstack.getMaxStackSize();
            int rem = itemstack.getCount() % itemstack.getMaxStackSize();
            for (int j = 0; j < stacks; j++) {
                InventoryHelper.spawnItemStack(world, pos.getX(), pos.getY(), pos.getZ(),
                    ItemHandlerHelper.copyStackWithSize(itemstack, itemstack.getMaxStackSize()));
            }
            InventoryHelper.spawnItemStack(world, pos.getX(), pos.getY(), pos.getZ(),
                ItemHandlerHelper.copyStackWithSize(itemstack, rem));
        }
    }



    private void validateSlotIndex(int slot) {
        if (slot < 0 || slot >= getSlots())
            throw new RuntimeException("Slot " + slot + " not in valid range - [0," + getSlots() + ")");
    }

    private static boolean areItemStacksGroupable(ItemStack a, ItemStack b) {
        if (a.isEmpty() || b.isEmpty()) return true;
        return a.getItem() == b.getItem() &&
            a.getItemDamage() == b.getItemDamage() &&
            ((a.getTagCompound() == null) == (b.getTagCompound() == null)) &&
            (a.getTagCompound() == null || a.getTagCompound().equals(b.getTagCompound())) &&
            a.areCapsCompatible(b);
    }



    @Override
    public int getSlots() {
        return Math.min(items.size() + 1, items.length());
    }

    @Nonnull
    @Override
    public ItemStack getStackInSlot(int slot) {
        validateSlotIndex(slot);
        return slot == items.size() ? ItemStack.EMPTY : items.get(slot);
    }

    @Nonnull
    @Override
    public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
        validateSlotIndex(slot);
        if (stack.isEmpty()) return ItemStack.EMPTY;
        else if (slot == items.size()) {
            if (slot > 0 && areItemStacksGroupable(items.get(slot - 1), stack)) slot--;
        } else if (slot == items.size() - 1) {
            if (!areItemStacksGroupable(items.get(slot), stack)) return stack;
        } else return stack;

        int before = slot == items.size() ? 0 : items.get(slot).getCount();
        int total = before + stack.getCount();
        int after = Math.min(total, capacity - count);
        if (!simulate) {
            items.set(slot, ItemHandlerHelper.copyStackWithSize(stack, after));
            count += after - before;
            updateParent();
        }
        return ItemHandlerHelper.copyStackWithSize(stack, total - after);
    }

    @Nonnull
    @Override
    public ItemStack extractItem(int slot, int amount, boolean simulate) {
        validateSlotIndex(slot);
        if (slot != 0) return ItemStack.EMPTY;
        if (amount == 0 || items.isEmpty()) return ItemStack.EMPTY;

        amount = Math.min(items.get(0).getMaxStackSize(), amount);
        ItemStack extracted = ItemHandlerHelper.copyStackWithSize(
            items.get(0), Math.min(amount, items.get(0).getCount()));
        if (!simulate) {
            count -= extracted.getCount();
            items.get(0).shrink(extracted.getCount());
            if (items.get(0).isEmpty()) items.remove(ItemStack.EMPTY);
            updateParent();
        }
        return extracted;
    }

    @Override
    public int getSlotLimit(int slot) {
        validateSlotIndex(slot);
        return Integer.MAX_VALUE;
    }

    @Override
    public NBTTagCompound serializeNBT() {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInteger("length", items.length());
        NBTTagList list = new NBTTagList();
        for (int i = 0; i < items.size(); i++) {
            list.appendTag(items.get(i).serializeNBT());
        }
        tag.setTag("items", list);
        tag.setInteger("capacity", capacity);
        tag.setInteger("count", count);
        return tag;
    }

    @Override
    public void deserializeNBT(NBTTagCompound tag) {
        items = new RingBuffer<>(tag.getInteger("length"), ItemStack.EMPTY);
        count = tag.getInteger("count");
        capacity = tag.getInteger("capacity");
        NBTTagList list = tag.getTagList("items", Constants.NBT.TAG_COMPOUND);
        for (int i = 0; i < list.tagCount(); i++) {
            items.set(i, new ItemStack(list.getCompoundTagAt(i)));
        }
    }

    protected void updateParent() {}
}
