package dev.stevendoesstuffs.distributor.container;

import java.util.Arrays;

public class RingBuffer<T> {
    private T[] buffer;
    private int start, size;

    public RingBuffer(int length, T placeholder) {
        buffer = (T[]) new Object[length];
        Arrays.fill(buffer, placeholder);
        start = 0;
    }

    public T get(int i) {
        if (i >= size) throw new IndexOutOfBoundsException();
        return buffer[(i + start) % buffer.length];
    }

    public void set(int i, T value) {
        if (i > size) throw new IndexOutOfBoundsException();

        buffer[(i + start) % buffer.length] = value;
        if (i == size) size++;
    }

    public T remove(T placeholder) {
        T value = buffer[start];
        buffer[start] = placeholder;
        start = (start + 1) % buffer.length;
        size--;
        return value;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public int length() {
        return buffer.length;
    }
}
