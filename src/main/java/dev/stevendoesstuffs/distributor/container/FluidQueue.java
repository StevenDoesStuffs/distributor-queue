package dev.stevendoesstuffs.distributor.container;

import dev.stevendoesstuffs.distributor.Util;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

import javax.annotation.Nullable;

public class FluidQueue implements IFluidHandler, INBTSerializable<NBTTagCompound> {
    private RingBuffer<FluidStack> fluids;
    private int capacity, count;

    public FluidQueue(int length, int capacity) {
        this.fluids = new RingBuffer<>(length, null);
        this.capacity = capacity;
        this.count = 0;
    }

    public int getCurrentAmount() {
        return fluids.isEmpty() ? 0 : fluids.get(0).amount;
    }

    public FluidStack copyCurrentFluid(int amount) {
        return Util.copyFluidStackWithSize(fluids.get(0), amount);
    }



    @Override
    public IFluidTankProperties[] getTankProperties() {
        IFluidTankProperties[] list = new IFluidTankProperties[fluids.length()];
        for (int i = 0; i < fluids.length(); i++) {
            list[i] = new FluidTankProperties(i < fluids.size() ? fluids.get(i) : null,
                capacity, i == fluids.size() - 1 || i == fluids.size(), i == 0);
        }
        return list;
    }

    @Override
    public int fill(FluidStack resource, boolean doFill) {
        int take = Math.min(capacity - count, resource.amount);
        if (fluids.isEmpty() || !fluids.get(fluids.size() - 1).isFluidEqual(resource)) {
            if (resource.amount == 0 || fluids.size() == fluids.length()) return 0;
            if (doFill) fluids.set(fluids.size(), Util.copyFluidStackWithSize(resource, take));
        } else {
            if (doFill) fluids.get(fluids.size() - 1).amount += take;
        }
        if (doFill) {
            count += take;
            updateParent();
        }
        return take;
    }

    @Nullable
    @Override
    public FluidStack drain(FluidStack resource, boolean doDrain) {
        if (fluids.isEmpty() || !fluids.get(0).isFluidEqual(resource)) return null;
        return drain(resource.amount, doDrain);
    }

    @Nullable
    @Override
    public FluidStack drain(int maxDrain, boolean doDrain) {
        if (fluids.isEmpty() || maxDrain == 0) return null;
        FluidStack first = fluids.get(0);
        int amount = Math.min(maxDrain, first.amount);
        if (doDrain) {
            count -= amount;
            first.amount -= amount;
            if (first.amount == 0) fluids.remove(null);
            updateParent();
        }
        return Util.copyFluidStackWithSize(first, amount);
    }


    @Override
    public NBTTagCompound serializeNBT() {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInteger("length", fluids.length());
        NBTTagList list = new NBTTagList();
        for (int i = 0; i < fluids.size(); i++) {
            list.appendTag(fluids.get(i).writeToNBT(new NBTTagCompound()));
        }
        tag.setTag("fluids", list);
        tag.setInteger("capacity", capacity);
        tag.setInteger("count", count);
        return tag;
    }

    @Override
    public void deserializeNBT(NBTTagCompound tag) {
        fluids = new RingBuffer<>(tag.getInteger("length"), null);
        count = tag.getInteger("count");
        capacity = tag.getInteger("capacity");
        NBTTagList list = tag.getTagList("fluids", Constants.NBT.TAG_COMPOUND);
        for (int i = 0; i < list.tagCount(); i++) {
            fluids.set(i, FluidStack.loadFluidStackFromNBT(list.getCompoundTagAt(i)));
        }
    }

    protected void updateParent() {}
}
