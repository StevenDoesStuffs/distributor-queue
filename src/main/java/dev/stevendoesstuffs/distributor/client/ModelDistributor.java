package dev.stevendoesstuffs.distributor.client;

import com.google.common.collect.ImmutableSet;
import dev.stevendoesstuffs.distributor.Distributor;
import dev.stevendoesstuffs.distributor.Util;
import net.minecraft.client.renderer.block.model.*;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ICustomModelLoader;
import net.minecraftforge.client.model.IModel;
import net.minecraftforge.client.model.ModelLoaderRegistry;
import net.minecraftforge.common.model.IModelState;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.function.Function;

public class ModelDistributor implements IModel {

    private static final ResourceLocation MODEL = new ResourceLocation(Distributor.MODID, "block/distributor");
    private static Collection<ResourceLocation> TEXTURES;

    private final EnumMap<EnumFacing, Boolean> outputs;

    public ModelDistributor(String variant) {
        this.outputs = new EnumMap<>(EnumFacing.class);
        String[] pairs = variant.split(",");
        for (String pair : pairs) {
            String[] kv = pair.split("=");
            EnumFacing face = EnumFacing.valueOf(kv[0].split("_")[1].toUpperCase());
            this.outputs.put(face, Boolean.valueOf(kv[1]));
        }
    }

    @Override
    public Collection<ResourceLocation> getTextures() {
        if (TEXTURES == null) {
            ImmutableSet.Builder<ResourceLocation> builder = ImmutableSet.builder();
            for (int i = 0; i < 1 << 6; i++) {
                builder.add(new ResourceLocation(Distributor.MODID, "blocks/distributor_side_" + i));
            }
            TEXTURES = builder.build();
        }
        return TEXTURES;
    }

    @Override
    public Collection<ResourceLocation> getDependencies() {
        return Arrays.asList(MODEL);
    }



    @Override
    public IBakedModel bake(IModelState state, VertexFormat format, Function<ResourceLocation, TextureAtlasSprite> bakedTextureGetter) {
        try {
            return ModelLoaderRegistry
                .getModel(MODEL)
                .bake(state, format, (loc) -> {
                    if (loc.getResourceDomain().equals(Distributor.MODID) &&
                        loc.getResourcePath().startsWith("blocks/distributor") &&
                        !loc.getResourcePath().equals("blocks/distributor_base")) {

                        EnumFacing face = EnumFacing.valueOf(loc.getResourcePath().split("_")[1].toUpperCase());
                        EnumFacing[] relative = Util.relative(face);
                        int index = 0;
                        for (int i = 0; i < 6; i++) {
                            if (outputs.get(relative[i])) index += 1 << i;
                        }
                        return bakedTextureGetter.apply(new ResourceLocation(
                            Distributor.MODID, "blocks/distributor_side_" + index));
                    }
                    else return bakedTextureGetter.apply(loc);
                });
        } catch (Exception e) {
            e.printStackTrace();
            return ModelLoaderRegistry.getMissingModel().bake(state, format, bakedTextureGetter);
        }
    }

    public static class LoaderModelDistributor implements ICustomModelLoader {

        @Override
        public void onResourceManagerReload(IResourceManager resourceManager) {}

        @Override
        public boolean accepts(ResourceLocation modelLocation) {
            return modelLocation instanceof ModelResourceLocation &&
                modelLocation.getResourceDomain().equals(Distributor.MODID) &&
                modelLocation.getResourcePath().equals("distributor");
        }

        @Override
        public IModel loadModel(ResourceLocation modelLocation) {
            return new ModelDistributor(((ModelResourceLocation) modelLocation).getVariant());
        }
    }
}
