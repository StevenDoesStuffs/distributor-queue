package dev.stevendoesstuffs.distributor.client;

import dev.stevendoesstuffs.distributor.Distributor;
import dev.stevendoesstuffs.distributor.RegistrationHandler;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.model.ModelLoaderRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(value = Side.CLIENT, modid = Distributor.MODID)
public class ModelRegistrationHandler {

    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent event) {
        ModelLoaderRegistry.registerLoader(new ModelDistributor.LoaderModelDistributor());

        Item item = Item.getItemFromBlock(RegistrationHandler.distributor);
        ModelLoader.setCustomModelResourceLocation(item, 0,
            new ModelResourceLocation(item.getRegistryName(), "inventory"));
    }

    @SubscribeEvent
    public static void registerSprites(TextureStitchEvent.Pre event) {
        for (int i = 0; i < 1 << 6; i++) {
            event.getMap().setTextureEntry(new DistributorSprite(i));
        }
    }
}
