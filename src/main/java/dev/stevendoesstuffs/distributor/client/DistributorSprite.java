package dev.stevendoesstuffs.distributor.client;

import com.google.common.collect.ImmutableSet;
import dev.stevendoesstuffs.distributor.Distributor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;

import java.util.Collection;
import java.util.function.Function;

public class DistributorSprite extends TextureAtlasSprite {

    private static final ResourceLocation
        BASE = new ResourceLocation(Distributor.MODID, "blocks/distributor_base"),
        ARROW = new ResourceLocation(Distributor.MODID, "blocks/distributor_arrow_output"),
        POINT = new ResourceLocation(Distributor.MODID, "blocks/distributor_point_output"),
        CORNERS = new ResourceLocation(Distributor.MODID, "blocks/distributor_corners_output");

    private static final Collection<ResourceLocation> TEXTURES = ImmutableSet.<ResourceLocation>builder()
        .add(BASE, ARROW, POINT, CORNERS)
        .build();

    private final int bitmask;

    public DistributorSprite(int bitmask) {
        super(new ResourceLocation(Distributor.MODID, "blocks/distributor_side_" + bitmask).toString());
        this.bitmask = bitmask;
    }

    @Override
    public Collection<ResourceLocation> getDependencies() {
        return TEXTURES;
    }

    @Override
    public boolean hasCustomLoader(IResourceManager manager, ResourceLocation location) {
        return true;
    }

    private static int mix(int a, int b) {
        if (a >> 24 == 0) return b;
        else if (a >>> 24 == 255) return a;
        double alpha_a = (a >>> 24) / 255.0;
        double alpha_b = (b >>> 24) / 255.0;
        double alpha = alpha_a + alpha_b*(1 - alpha_a);

        int color = (int) Math.round(255 * alpha) << 24;
        for (int i = 0; i < 3; i++) {
            double color_a = ((a >>> (8 * i)) & 255) / 255.0;
            double color_b = ((b >>> (8 * i)) & 255) / 255.0;

            color += (int) Math.round(255 * (color_a*alpha_a + color_b*alpha_b*(1 - alpha_a) / alpha)) << (8 * i);
        }
        return color;
    }

    @Override
    public boolean load(IResourceManager manager, ResourceLocation location, Function<ResourceLocation, TextureAtlasSprite> textureGetter) {
        TextureAtlasSprite base = textureGetter.apply(BASE);
        width = base.getIconWidth();
        height = base.getIconHeight();
        int[] pixels = base.getFrameTextureData(0)[0].clone();

        int[] arrow = textureGetter.apply(ARROW).getFrameTextureData(0)[0];
        for (int rot = 0; rot < 4; rot++) {
            if ((bitmask & (1 << rot)) != 0) {
                for (int i = 0; i < width * height; i++) {
                    int r = i / width;
                    int c = i % width;
                    // rotate the coordinates counterclockwise
                    // to rotate the image clockwise
                    for (int q = 0; q < rot; q++) {
                        int r1 = r;
                        r = width - 1 - c;
                        c = r1;
                    }
                    int j = c + width * r;
                    pixels[i] = mix(arrow[j], pixels[i]);
                }
            }
        }

        int[] point = textureGetter.apply(POINT).getFrameTextureData(0)[0];
        if ((bitmask & (1 << 4)) != 0) {
            for (int i = 0; i < width * height; i++)
                pixels[i] = mix(point[i], pixels[i]);
        }

        int[] corners = textureGetter.apply(CORNERS).getFrameTextureData(0)[0];
        if ((bitmask & (1 << 5)) != 0) {
            for (int i = 0; i < width * height; i++)
                pixels[i] = mix(corners[i], pixels[i]);
        }

        this.clearFramesTextureData();
        int[][] result = new int[Minecraft.getMinecraft().gameSettings.mipmapLevels + 1][];
        result[0] = pixels;
        this.framesTextureData.add(result);
        return false;
    }
}
