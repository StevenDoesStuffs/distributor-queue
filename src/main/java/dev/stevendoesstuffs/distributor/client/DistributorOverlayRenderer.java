package dev.stevendoesstuffs.distributor.client;

import codechicken.lib.vec.Vector3;
import dev.stevendoesstuffs.distributor.Distributor;
import dev.stevendoesstuffs.distributor.block.BlockDistributor;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

// code adapted from https://github.com/GregTechCE/GregTech/blob/master/src/main/java/gregtech/common/render/WrenchOverlayRenderer.java
// see license here https://github.com/GregTechCE/GregTech/blob/master/LICENSE
@Mod.EventBusSubscriber(value = Side.CLIENT, modid = Distributor.MODID)
public class DistributorOverlayRenderer {

    @SubscribeEvent
    public static void onDrawBlockHighlight(DrawBlockHighlightEvent event) {

        EntityPlayer player = event.getPlayer();
        RayTraceResult target = event.getTarget();
        World world = player.world;

        if (target.typeOfHit != RayTraceResult.Type.BLOCK) return;
        BlockPos pos = target.getBlockPos();
        IBlockState blockState = world.getBlockState(pos);

        if (player.getHeldItem(EnumHand.MAIN_HAND).isEmpty() &&
            player.getHeldItem(EnumHand.OFF_HAND).isEmpty() &&
            blockState.getBlock() instanceof BlockDistributor) {

            EnumFacing facing = target.sideHit;
            GlStateManager.enableBlend();
            GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
            GlStateManager.glLineWidth(2.0F);
            GlStateManager.disableTexture2D();
            GlStateManager.depthMask(false);

            if (world.getWorldBorder().contains(pos)) {
                double d3 = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double) event.getPartialTicks();
                double d4 = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double) event.getPartialTicks();
                double d5 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double) event.getPartialTicks();
                AxisAlignedBB box = blockState.getSelectedBoundingBox(world, pos).grow(0.002D).offset(-d3, -d4, -d5);
                RenderGlobal.drawSelectionBoundingBox(box, 0.0F, 0.0F, 0.0F, 0.4F);
                drawOverlayLines(facing, box);
            }

            GlStateManager.depthMask(true);
            GlStateManager.enableTexture2D();
            GlStateManager.disableBlend();
            event.setCanceled(true);
        }
    }

    private static void drawOverlayLines(EnumFacing facing, AxisAlignedBB box) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        buffer.begin(3, DefaultVertexFormats.POSITION_COLOR);

        Vector3 origin = new Vector3(box.minX, box.minY, box.maxZ);
        Vector3 posX = new Vector3(box.maxX, box.minY, box.maxZ);
        Vector3 posY = new Vector3(box.minX, box.maxY, box.maxZ);
        Vector3 cubeCenter = new Vector3(box.getCenter());

        posX.subtract(cubeCenter);
        origin.subtract(cubeCenter);
        posY.subtract(cubeCenter);

        double rot = 0.0;
        switch(facing) {
            case DOWN:
            case WEST: rot = Math.PI / 2; break;
            case UP:
            case EAST: rot = -Math.PI / 2; break;
            case NORTH: rot = Math.PI; break;
        }
        Vector3 axis = facing.getAxis().isVertical() ? new Vector3(1, 0, 0) : Vector3.down;
        origin.rotate(rot, axis);
        posX.rotate(rot, axis);
        posY.rotate(rot, axis);

        posX.subtract(origin);
        posY.subtract(origin);
        origin.add(cubeCenter);

        drawLine(buffer, origin, posX, posY, 5, 5, 11, 5);
        drawLine(buffer, origin, posX, posY, 5, 5, 5, 11);
        drawLine(buffer, origin, posX, posY, 11, 5, 11, 11);
        drawLine(buffer, origin, posX, posY, 5, 11, 11, 11);

        drawLine(buffer, origin, posX, posY, 5, 5, 4, 4);
        drawLine(buffer, origin, posX, posY, 11, 5, 12, 4);
        drawLine(buffer, origin, posX, posY, 5, 11, 4, 12);
        drawLine(buffer, origin, posX, posY, 11, 11, 12, 12);

        drawLine(buffer, origin, posX, posY, 4, 4, 0, 4);
        drawLine(buffer, origin, posX, posY, 4, 4, 4, 0);
        drawLine(buffer, origin, posX, posY, 12, 4, 16, 4);
        drawLine(buffer, origin, posX, posY, 12, 4, 12, 0);
        drawLine(buffer, origin, posX, posY, 4, 12, 0, 12);
        drawLine(buffer, origin, posX, posY, 4, 12, 4, 16);
        drawLine(buffer, origin, posX, posY, 12, 12, 16, 12);
        drawLine(buffer, origin, posX, posY, 12, 12, 12, 16);

        tessellator.draw();
    }

    private static void drawLine(BufferBuilder buffer,
        Vector3 origin, Vector3 posX, Vector3 posY,
        int x1Int, int y1Int, int x2Int, int y2Int) {

        double x1 = x1Int / 16.0;
        double y1 = y1Int / 16.0;
        double x2 = x2Int / 16.0;
        double y2 = y2Int / 16.0;

        buffer
            .pos(origin.x + x1*posX.x + y1*posY.x,
                origin.y + x1*posX.y + y1*posY.y,
                origin.z + x1*posX.z + y1*posY.z)
            .color(0, 0, 0, 0.0F)
            .endVertex();

        buffer
            .pos(origin.x + x2*posX.x + y2*posY.x,
                origin.y + x2*posX.y + y2*posY.y,
                origin.z + x2*posX.z + y2*posY.z)
            .color(0, 0, 0, 0.5F)
            .endVertex();
    }
}
