package dev.stevendoesstuffs.distributor;

import net.minecraftforge.common.config.Config;

@Config(modid = Distributor.MODID)
public class DistributorConfig {
    @Config.Comment("The number of types a distributor can store")
    @Config.RangeInt(min = 1)
    public static int itemQueueTypes = 27;

    @Config.Comment("The total number of items a distributor can store")
    @Config.RangeInt(min = 1)
    public static int itemQueueCapacity = 64 * itemQueueTypes;

    @Config.Comment("The number of types a distributor can store")
    @Config.RangeInt(min = 1)
    public static int fluidQueueTypes = 9;

    @Config.Comment("The total amount of mB a distributor can store")
    @Config.RangeInt(min = 1)
    public static int fluidQueueCapacity = 64000 * fluidQueueTypes;
}
