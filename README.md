# Distributor

**Requires `codechickenlib`.**

This mod adds a single block called the distributor. 

The distributor is a precise item and fluid splitter queue.
Any items or fluids pumped into the distributor will be output to neighboring blocks on active sides equally and in order.
The active sides are configured by shift-clicking the distributor with an empty hand.

It will never split unevenly, so if the block can't push an equal amount out to all sides, it will do nothing.
For example, if there are four output sides and three items in the distributor, the distributor will block.
Similarly, if there are four items and four active sides, 
but one of those sides can't accept items at the moment (e.g. it's full), the distributor will also block.

The items are output in FIFO order. 
If four coal, four charcoal, and then four coal are pumped into the distributor,
the distributor will need to distribute the first four coal, then the four charcoal, then finally the last four coal.
This is in contrast to normal chests, where the two groups of four coal will stack when possible.

This block will get stuck in many situations. It's up to you to make sure that doesn't happen.

The use case this mod was designed for is splitting items and fluids between machines for AE2 autocrafting.
This block will work correctly with AE2's semi-broken blocking mode. 
